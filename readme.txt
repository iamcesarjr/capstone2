TEST ACCOUNTS:

*Regular User: 
	email: luffy@gmail.com
	pwd: user123
*Admin User:
	email: cesar@gmail.com
	pwd: admin123

ROUTES: (USER)

*USER REGISTRATION (POST)
http://localhost:4000/users/register
request body:
	firstName (String)
	lastName(String)
	email (String)
	mobileNo (String)
	password (String)

*USER AUTHENTICATION (POST)
http://localhost:4000/users/login
request body:
	email(String)
	password (String)

*RETRIEVE USER DETAILS (GET)
http://localhost:4000/users/profile
request body: 
	userId (String)

*UPDATE TO ADMIN (ADMIN ACCESS REQUIRED) (PUT)
http://localhost:4000/users/updateAdmin
request body::
	userId (String)

*DELETE USER (ADMIN ACCESS REQUIRED) (DEL)
http://localhost:4000/users/deleteUser
request body::
	userId (String)



ROUTES: (PRODUCT)

*CREATE NEW PRODUCT (ADMIN ACCESS REQUIRED) (POST)
http://localhost:4000/products/addProduct
request body:
	name(String)
	description (String)
	price (Number)
	stock (Number)

*RETRIEVE ALL PRODUCTS (ADMIN ACCESS REQUIRED) (GET)
http://localhost:4000/products/all
request body: none

*RETRIEVE ALL ACTIVE PRODUCTS (GET)
http://localhost:4000/products/active
request body: none

*RETRIEVE SPECIFIC PRODUCT BY ID (GET)
http://localhost:4000/products/:productId
request body: none

*UPDATE PRODUCT INFORMATION (ADMIN ACCESS REQUIRED) (PUT)
http://localhost:4000/products/:productId/update
request body:
	name(String)
	description (String)
	price (Number)
	stock (Number)

*ARCHIVE A SPECIFIC PRODUCT (ADMIN ACCESS REQUIRED) (PUT)
http://localhost:4000/products/:productId/archive
request body: none

*SET O ACTIVE A SPECIFIC PRODUCT (ADMIN ACCESS REQUIRED) (PUT)
http://localhost:4000/products/:productId/active
request body: none

*SEARCH A SPECIFIC PRODUCT BY PRODUCT NAME (POST)
http://localhost:4000/products/search
request body:
	name(String)

*DELETE A SPECIFIC PRODUCT (ADMIN ACCESS REQUIRED) (DEL)
http://localhost:4000/products/deleteProduct
request body:
	productId (String)


ROUTES: (ORDER)

*CREATE A NEW ORDER (POST)
http://localhost:4000/orders/createOrder
request body:
	userId (String)
	products
		productId (String)
		productName (String)
		quantity (String)

*RETRIEVE ALL ORDERS (ADMIN ACCESS REQUIRED) (GET)
http://localhost:4000/orders/allOrders
request body: none

*RETRIEVE USER'S ORDER BY BY ID (GET)
http://localhost:4000/orders/:orderId
request body: none

*RETRIEVE USER'S ORDER BY NAME (ADMIN ACCESS REQUIRED) (POST)
http://localhost:4000/orders/userOrder
request body:
	name (String)



