// Dependencies & Modules
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

//Routes
const userRoutes = require("./routes/user")
const productRoutes = require("./routes/product")

//Environment Setup
const port = 4000;

//Server Setup
const app = express();

app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use(cors());

//Database Connection
mongoose.connect("mongodb+srv://iamcesarjr:admin123@batch-297.ebua14s.mongodb.net/capstone2?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
)

	//PROMPT
	let db = mongoose.connection;
	db.on('error', console.error.bind(console,'Connection error'));
	db.once('open', () => console.log('Connected to MongoDB Atlas.'));



//[Back-end Routes]
app.use("/users",userRoutes)
app.use("/products",productRoutes)
app.use('/images', express.static(__dirname + '/images'));

//Server Gateway Response
if(require.main === module){
	app.listen(process.env.PORT || port, ()=>{
		console.log(`API is now online on port ${process.env.PORT || port}`)
	})
}

module.exports = {app,mongoose}