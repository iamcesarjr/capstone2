//[Product Model]

const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({

	name: {
		type: String,
		required: [true, "Product Name is required"]
	},
	description: {
		type: String,
		required: [true, "Description is required"]
	},
	price: {
		type: Number,
		required: [true, "Price is required"]
	},
	category: {
		type: String,
		required: [true, "Category is required"]
	},	
	stock: {
		type: Number,
		required: [true, "Price is required"]
	},
	isActive: {
		type: Boolean,
		default: true
	},
	images: [{
		type: String
	}],
	createdOn: {
		type: Date,
		default: new Date()
	},
	userOrders: [
		{
			userId: {
					type: String,
					required: [true, "productId is required"]
				},
			quantity:{
					type: Number,
					default:0
			},
			orderDate:{
				type: Date,
				default: new Date()
			}
		}
		]

})

//Model
module.exports = mongoose.model("Product",productSchema);