//[User Model]

const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({

	firstName: {
		type: String,
		required: [true, "First Name is required"]
	},
	lastName: {
		type: String,
		required: [true, "Last Name is required"]
	},
	mobileNo: {
		type: String,
		required: [true, "Mobile Number is required"]
	},
	email: {
		type: String,
		required: [true, "Email is required"]
	},
	password : {
		type: String,
		required: [true, "Password is required"]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},

	cart: [
		{
			productId: {
				type: String,
				required: [true, "productId is required"]
			},
			name :{
				type: String,
				required: [true, "Product Name is required"]
			},
			price: {
				type: Number,
				default: 0
			},
			quantity: {
				type: Number,
				default: 0
			},
			subtotal: {
				type: Number,
				default: 0
			}
		}
	],
 
	orderPlaced: [
		{
			productId: {
				type: String,
				required: [true, "productId is required"]
			},
			name: {
				type: String,
				required: [true, "Product Name is required"]
			},
			orderPlaced: {
				type: Date,
				default : new Date()
			},
			quantity: {
				type: Number,
				default: 0
			},
			status: {
				type: String,
				default: "PENDING"
			},
			 totalPrice: {
	        type: Number,
	        default: 0
		}
		}

	]

})

//Model
module.exports = mongoose.model("User",userSchema);