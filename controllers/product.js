//Dependencies and Modules of Product Controller

const Product = require("../models/Product");
const User = require("../models/User");

//[Create New Product - Admin Access]
module.exports.addProduct = (req,res) =>{

	let newProduct = new Product({

		name: req.body.name,
		description: req.body.description,
		price: req.body.price,
		category : req.body.category,
		stock: req.body.stock,
		images: req.body.images,
	})

	return newProduct.save().then((user,error) => {

		if(error){
			return res.send(false)
		}else{
			return res.send(true)
		}
	})
	.catch(err => res.send(err))
};

//[Retrieving All Products]
module.exports.getAllProducts = (req,res) => {

	return Product.find({}).then(result => res.send(result)).catch(err => res.send(err))
};

//[Retrieving All Active Products]

module.exports.getAllActiveProducts = (req,res) => {

	return Product.find({isActive:true}).then(result => res.send(result)).catch(err => res.send(err))
};


//[Retrieving Specific Products]
module.exports.getProduct = (req,res) => {

	return Product.findById(req.params.productId).then(result => res.send(result)).catch(err => res.send(err))
};


//[Updating Product Information - Admin Access]
module.exports.updateProduct = (req, res) => {
  const updatedProduct = {
    name: req.body.name,
    description: req.body.description,
    price: req.body.price,
    stock: req.body.stock,
  };

  if (req.body.images && req.body.images.length > 0) {
    // If new images are provided, update the 'images' field
    updatedProduct.images = req.body.images;
  }

  Product.findByIdAndUpdate(req.params.productId, updatedProduct)
    .then((product, error) => {
      if (error) {
        return res.send(false);
      } else {
        return res.send(true);
      }
    })
    .catch((err) => res.send(err));
};

//[Set Product to ARCHIVE Using productId - Admin Access]
module.exports.archiveProduct = (req,res) => {

	return Product.findByIdAndUpdate(req.params.productId, {isActive:false}).then((product,error) => {

		if(error){
			return res.send(false);
		}else{
			return res.send(true)
		}
	})
	.catch(err => res.send(err))
}

//[Set Product to ACTIVE Using productId - Admin Access]
module.exports.activeProduct = (req,res) => {

	return Product.findByIdAndUpdate(req.params.productId, {isActive:true}).then((product,error) => {

		if(error){
			return res.send(false);
		}else{
			return res.send(true)
		}
	})
	.catch(err => res.send(err))
};

//[Search a Specific Product by its Name]
module.exports.searchProductByName = (req,res) => {

	const productName = req.body.name;

	Product.find({name:{$regex:productName, $options:'i'}}).then(result => res.send(result)).catch(err => res.send(err))
}

//[SEARCH PRODUCT BY PRICE]
	exports.searchProductsByPriceRange = async (req, res) => {
		try {
		  const { minPrice, maxPrice } = req.body;
	  
		  const products = await Product.find({
			price: { $gte: minPrice, $lte: maxPrice }
		  });
	  
		  res.status(200).json({ products });
		} catch (error) {
		  res.status(500).json({ error: 'An error occurred while searching for products' });
		}
	  };

//[DELETE SPECIFIC PRODUCT - ADMIN ACCESS ONLY]
module.exports.deleteProduct = (req,res) =>{

	Product.findByIdAndRemove(req.params.productId).then(result =>{
		if(result === null){
			return res.send(false)
		}else{
			return res.send(true)
		}
	})
	.catch(err => res.send(err))
};
