//Dependencies and Modules of User Controller

const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");
const Product = require("../models/Product")


//[USER REGISTRATION]
module.exports.registerUser = async (reqbody) => {
  try {

    let newUser = new User({
      firstName: reqbody.firstName,
      lastName: reqbody.lastName,
      email: reqbody.email,
      mobileNo: reqbody.mobileNo,
      password: bcrypt.hashSync(reqbody.password, 10),

    });

    const user = await newUser.save();

    if (user) {
      return true;
    } else {
      return false;
    }
  } catch (error) {
    console.error(error);
    return "User registration failed";
  }
};

//[Retrieving All Users]
module.exports.getAllUsers = (req,res) => {

  return User.find({})
  .then(result => {
    console.log(result)
    res.send(result)
  })
  .catch(err => res.send(err))
};

//[RETRIEVE USER DETAILS]
    module.exports.getProfile = (req, res) => {

    return User.findById(req.user.id)
    .then(result => {
      result.password = "";
      return res.send(result);
    })
    .catch(err => res.send(err))
  };

//[USER AUTHENTICATION]
module.exports.loginUser = (req,res) => {

	return User.findOne({email:req.body.email}).then(result => {
		if(result === null){
			return false
		}else{
			const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password)

			if(isPasswordCorrect){
				return res.send({access: auth.createAccessToken(result)})
			}else{
				return res.send(false)
			}
		}
	})
	.catch(err => res.send(err))
};


//[RETRIEVE USER DETAILS BY NAME]
module.exports.searchUserByName = (req, res) => {
  const { firstName, lastName } = req.body;

  User.find({
    $or: [
      { firstName: { $regex: firstName, $options: 'i' } },
      { lastName: { $regex: lastName, $options: 'i' } },
    ],
  })
    .then(result => res.send(result))
    .catch(err => res.send(err));
  }

  //[SECTION] Reset Profile
  module.exports.updateProfile = async (req, res) => {
    try {

      console.log(req.user);
      console.log(req.body);
      const userId = req.user.id;

      const { firstName, lastName, mobileNo } = req.body;

        const updatedUser = await User.findByIdAndUpdate(
        userId,
        { firstName, lastName, mobileNo },
        { new: true }
        );
      
        res.send(updatedUser);
      } catch (error) {
        console.error(error);
        res.status(500).send({ message: 'Failed to update profile' });
      }
    }


//[UPDATE USER ACCESS TO ADMIN - ADMIN ACCESS ONLY]
module.exports.updateToAdmin = (req,res) =>{

	return User.findByIdAndUpdate(req.params.userId, {isAdmin:true}).then((result, error) => {
		if(error){
      return res.send(false);
    }else{
      return res.send(true)
    }
	})
	.catch(err => res.send(err))
}

//[REMOVE ADMIN ACCESS - ADMIN ACCESS ONLY]
module.exports.removeAdminAccess = (req,res) =>{

  return User.findByIdAndUpdate(req.params.userId, {isAdmin:false}).then((result, error) => {
    if(error){
      return res.send(false);
    }else{
      return res.send(true)
    }
  })
  .catch(err => res.send(err))
}


//[DELETE SPECIFIC USER - ADMIN ACCESS ONLY]
module.exports.deleteUser = (req,res) =>{

	User.findByIdAndRemove(req.body.userId).then(result =>{
		if(result === null){
			return res.send(false)
		}else{
			return res.send(true)
		}
	})
	.catch(err => res.send(err))
}

//[ADD TO CART FUNCTION]
module.exports.addToCart = async (req, res) => {
  try {
    const { productId, name, quantity, price } = req.body;
    const userId = req.user.id;

    const user = await User.findById(userId);

    if (!user) {
      return res.send(false)
    }

    const product = await Product.findById(req.body.productId).exec()

    if(!product){
    	return res.send(false)
    }

    if(!product.isActive){
    	return res.send(false)
    }

    if (product.quantity <= 0) {
      return res.send(false);
    }

    if (product.stock <= 0) {
	  return res.send(false);
		} else if (req.body.quantity > product.stock) {
	  return res.send(false);
		}

    const newProduct = {
      productId,
      name,
      price,
      quantity,
      subtotal: calculateSubtotal(price, quantity)
    };

    user.cart.push(newProduct);
    await user.save();

    res.send(true)
  } catch (error) {
    res.send(error)
  }
};

//[GET USER'S CART]
module.exports.getUserCart = (req,res) => {

  const userId = req.user.id

  User.findById(userId)
  .then(user => {
    if(!user){
      return res.status(404).send("User not found");
    }
    res.send(user.cart);
  })
  .catch(err => console.log(err))
}

//[INCREASE QUANTITY OF PRODUCTS ON CART]
module.exports.increaseCartItemQuantity = (req, res) => {
  const productId = req.params.productId;

  const userId = req.user.id;

  User.findById(userId)
    .then(user => {
      if (!user) {
        return res.status(404).send("User not found");
      }

      // Find the cart item with the given productId
      const cartItem = user.cart.find(item => item.productId === productId);
      console.log(cartItem)

      cartItem.quantity += 1;
      cartItem.subtotal = cartItem.price * cartItem.quantity;
      console.log(cartItem)


      user.save()
        .then(() => {
          res.send(true);
          console.log(cartItem)
        })
        .catch(error => {
          console.error(error);
          res.status(500).send("Internal Server Error");
        });
    })
    .catch(error => {
      console.error(error);
      res.status(500).send("Internal Server Error");
    });
};

//[DECREASE QUANTITY OF PRODUCTS ON CART]
exports.decreaseCartItemQuantity = async (req, res) => {
  const productId = req.params.productId;

  const userId = req.user.id;

  User.findById(userId)
    .then(user => {
      if (!user) {
        return res.status(404).send("User not found");
      }

      // Find the cart item with the given productId
      const cartItem = user.cart.find(item => item.productId === productId);
      console.log(cartItem)

      cartItem.quantity -= 1;
      cartItem.subtotal = cartItem.price * cartItem.quantity;
      console.log(cartItem)


      user.save()
        .then(() => {
          res.send(true);
          console.log(cartItem)
        })
        .catch(error => {
          console.error(error);
          res.status(500).send("Internal Server Error");
        });
    })
    .catch(error => {
      console.error(error);
      res.status(500).send("Internal Server Error");
    });
};

async function getProductPrice(productId) {
  try {
    const result = await Product.findById(productId);
    if (result) {
      return result.price;
    } else {
      throw new Error("Product not found");
    }
  } catch (error) {
    console.error(error);
    return null;
  }
}

function calculateSubtotal(price, quantity) {
  return price * quantity;
}


//[DELETE PRODUCT ON THE CART BY ENTERING PRODUCT ID]
module.exports.deleteItem = async (req, res) => {
  try {
    const productId = req.params.productId
    const userId = req.user.id;

    const user = await User.findById(userId);

    if (!user) {
      return res.send("User does not exist!");
    }

    const indexToRemove = user.cart.findIndex(item => item.productId === productId);

    if (indexToRemove === -1) {
      return res.send(`Product ID ${productId} not found on cart`);
    }

    user.cart.splice(indexToRemove, 1);
    await user.save();

    return res.send(true);
  } catch (error) {
    console.error(error);
    return res.send("Internal Server Error");
  }
};

//[UPDATE THE USER'S PASSWORD]
  module.exports.resetPassword = async (req, res) => {
    try {

    //console.log(req.user)
    //console.log(req.body)

      const { newPassword } = req.body;
      const { id } = req.user; // Extracting user ID from the authorization header
    
      // Hashing the new password
      const hashedPassword = await bcrypt.hash(newPassword, 10);
    
      // Updating the user's password in the database
      await User.findByIdAndUpdate(id, { password: hashedPassword });
    
      // Sending a success response
      res.status(200).send({ message: 'Password reset successfully' });
    } catch (error) {
      console.error(error);
      res.status(500).send({ message: 'Internal server error' });
    }
  };


  //[UPDATE USER PROFILE]
  module.exports.updateProfile = async (req, res) => {
    try {

      console.log(req.user);
      console.log(req.body);
      const userId = req.user.id;

      const { firstName, lastName, mobileNo } = req.body;
    
      const updatedUser = await User.findByIdAndUpdate(
      userId,
      { firstName, lastName, mobileNo },
      { new: true }
      );
    
      res.send(updatedUser);
    } catch (error) {
      console.error(error);
      res.status(500).send({ message: 'Failed to update profile' });
    }
    }


    //[CONFIRM ORDER FUNCTION]
module.exports.confirmOrder = async (req, res) => {
  try {
    const userId = req.user.id;

    const user = await User.findById(userId);

    if (!user) {
      return res.send('User does not exist!');
    }

    const orderProducts = user.cart.map(cartItem => ({
      productId: cartItem.productId,
      name: cartItem.name,
      orderPlaced: new Date(),
      quantity: cartItem.quantity,
      status: 'PENDING',
      totalPrice: cartItem.price * cartItem.quantity
    }));

    const totalOrderPrice = orderProducts.reduce((total, product) => total + product.totalPrice, 0);

    user.orderPlaced.push(...orderProducts);

    // Embed order details in the Product model
    for (const cartItem of user.cart) {
      const product = await Product.findById(cartItem.productId);

      if (product) {
        product.userOrders.push({
          userId: userId,
          quantity: cartItem.quantity,
          orderDate: new Date()
        });

        await product.save();
      }
    }

    user.cart = [];

    await user.save();

    res.send(true);
  } catch (error) {
    console.error(error);
    res.send('Internal Server Error');
  }
};


//[RETRIEVE ORDERS PLACED BY USER]
module.exports.getUserOrder = async (req, res) => {
  try {
    const userId = req.user.id;

    const user = await User.findById(userId);

    if (!user) {
      return res.send('User does not exist');
    } else {
      console.log(user);
      return res.send(user.orderPlaced);
    }
  } catch (error) {
    console.error(error);
    return res.send('Internal Server Error');
  }
};


//[RETRIEVE ALL ORDERS - ADMIN ACCESS]
  module.exports.getAllOrders = (req, res) =>{

    User.find({}).then(result => res.send(result)).catch(err => res.send(err))
  }

  //[UPDATE ORDERS STATUS TO SHIPPING - ADMIN ACCESS ONLY]
module.exports.updateToShipping = (req, res) => {
  const userId = req.params.userId; // Correctly extract userId from route parameter
  const productId = req.params.productId; // Correctly extract productId from route parameter

  User.findOneAndUpdate(
    { 
      _id: userId, // Use _id to identify the user
      'orderPlaced.productId': productId // Match the specific order by productId
    },
    { $set: { 'orderPlaced.$.status': 'AWAIT SHIPPING' } }, // Update the matched order status to 'AWAIT SHIPPING'
    { new: true }
  )
    .then((updatedUser) => {
      if (!updatedUser) {
        return res.send(false)
      }
      res.send(true)
    })
    .catch((error) => {
      console.error('Error updating status:', error);
      res.status(500).json({ success: false, message: 'Internal server error' });
    });
};


  //[UPDATE ORDERS STATUS TO DELIVERED - ADMIN ACCESS ONLY]
module.exports.updateToDelivered = (req, res) => {
  const userId = req.params.userId; // Correctly extract userId from route parameter
  const productId = req.params.productId; // Correctly extract productId from route parameter

  User.findOneAndUpdate(
    { 
      _id: userId, // Use _id to identify the user
      'orderPlaced.productId': productId
    },
    { $set: { 'orderPlaced.$.status': 'DELIVERED' } }, // Use $ to update the matched order status
    { new: true }
  )
    .then((updatedUser) => {
      if (!updatedUser) {
        return res.send(false)
      }
      res.send(true)
    })
    .catch((error) => {
      console.error('Error updating status:', error);
      res.status(500).json({ success: false, message: 'Internal server error' });
    });
};


  //[UPDATE ORDERS STATUS TO CANCELLED - ADMIN ACCESS ONLY]
module.exports.updateToCancelled = (req, res) => {
  const userId = req.params.userId; // Correctly extract userId from route parameter
  const productId = req.params.productId; // Correctly extract productId from route parameter

  User.findOneAndUpdate(
    { 
      _id: userId, // Use _id to identify the user
      'orderPlaced.productId': productId
    },
    { $set: { 'orderPlaced.$.status': 'CANCELLED' } }, // Use $ to update the matched order status
    { new: true }
  )
    .then((updatedUser) => {
      if (!updatedUser) {
        return res.send(false)
      }
      res.send(true)
    })
    .catch((error) => {
      console.error('Error updating status:', error);
      res.status(500).json({ success: false, message: 'Internal server error' });
    });
};
