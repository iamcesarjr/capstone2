//Product Routes

//Dependecies & Modules
const express = require("express");
const productController = require("../controllers/product");
const auth = require("../auth");
const { verify, verifyAdmin } = auth;


//Routing Component
const router = express.Router();

//[CREATING NEW PRODUCT - ADMIN ACCESS ONLY]
router.post("/", verify, verifyAdmin, productController.addProduct);

//[RETRIEVE ALL PRODUCTS]
router.get("/all", productController.getAllProducts);

//[RETRIEVE ALL ACTIVE PRODUCTS]
router.get("/", productController.getAllActiveProducts);

//[RETRIEVE A SPECIFIC PRODUCT]
router.get("/:productId", productController.getProduct);

//[UPDATE PRODUCT INFORMATION - ADMIN ACCESS ONLY]
router.put("/:productId", verify, verifyAdmin, productController.updateProduct);

//[SET PRODUCT TO ARCHIVE - ADMIN ACCESS ONLY]
router.put("/:productId/archive", verify, verifyAdmin, productController.archiveProduct);

//[SET PRODUCT TO ACTIVE - ADMIN ACCESS ONLY]
router.put("/:productId/activate", verify, verifyAdmin, productController.activeProduct);

//[SEARCH PRODUCT BY NAME]
router.post("/searchByName", productController.searchProductByName);

//[SEARCH PRODUCT BY PRICE]
	router.post('/searchByPrice', productController.searchProductsByPriceRange);

//[DELETE SPECIFIC USER - ADMIN ACCESS ONLY]
router.delete("/:productId/delete", verify, verifyAdmin, productController.deleteProduct)


// Export Route System

module.exports = router;