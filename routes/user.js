//Dependencies and Modules ofr User Routes

const express = require("express");
const userController = require("../controllers/user");
const auth = require("../auth");
const multer = require("multer");
const path = require("path")
const { verify, verifyAdmin } = auth;
const User = require('../models/User');

//Routing Component
const router = express.Router();

//[USER REGISTRATION]
router.post("/register", (req,res)=>{
	userController.registerUser(req.body).then(resultFromController=>res.send(resultFromController))
})

//[USER AUTHENTICATION]
router.post("/login",userController.loginUser);

//[RETRIEVE USER DETAILS]
router.get("/details", verify, userController.getProfile);

//[RETRIEVE USER DETAILS]
router.get("/all",userController.getAllUsers);

//[RETRIEVE SPECIFIC USER - ADMIN ACCESS]
router.post("/search" , verify, verifyAdmin, userController.searchUserByName)

//[SECTION] Update Profile	
router.put('/profile', verify, userController.updateProfile);

//[CHANGE USER ACCESS TO ADMIN]
router.put("/:userId/updateAdmin", verify, verifyAdmin, userController.updateToAdmin);

//[CHANGE USER ACCESS TO ADMIN]
router.put("/:userId/removeAdmin", verify, verifyAdmin, userController.removeAdminAccess);

//[DELETE SPECIFIC USER - ADMIN ACCESS ONLY]
router.delete("/deleteUser", verify, verifyAdmin, userController.deleteUser);

//[ADD TO CART FUNCTION]
router.post("/addToCart", verify, userController.addToCart);

//[GET USER CART DETAILS]
router.get("/cart", verify, userController.getUserCart);

//[INCREASE QUANTITY OF PRODUCT ON CART]
router.put("/cart/:productId/increase",verify, userController.increaseCartItemQuantity)

//[DECREASE QUANTITY OF PRODUCT ON CART]
router.put("/cart/:productId/decrease", verify, userController.decreaseCartItemQuantity)

//[SECTION] Reset Password
router.put('/reset-password', verify, userController.resetPassword);

//[DELETE PRODUCT ON THE CART BY ENTERING PRODUCT ID]
router.delete("/cart/:productId/deleteItem", verify, userController.deleteItem)

//[CREATE ORDER UPON CONFIRMING PRODUCTS ON CART]
router.post("/confirmOrder", verify, userController.confirmOrder)

//[RETRIEVE SPECIFIC USER'S ORDER BY USING ORDER ID AS ENDPOINT]
router.get("/:userId/orders", verify, userController.getUserOrder)

//[RETRIEVE SPECIFIC USER'S ORDER BY USING ORDER ID AS ENDPOINT]
router.get("/allOrders", verify, verifyAdmin, userController.getAllOrders)

//[UPDATE ORDER STATUS - ADMIN ACCESS]
router.put("/:userId/:productId/updateShipping", verify, verifyAdmin, userController.updateToShipping)

//[UPDATE ORDER STATUS - ADMIN ACCESS]
router.put("/:userId/:productId/updateDelivered", verify, verifyAdmin, userController.updateToDelivered)

//[UPDATE ORDER STATUS - ADMIN ACCESS]
router.put("/:userId/:productId/updateCancelled", verify, verifyAdmin, userController.updateToCancelled)

// Export Route System
module.exports = router;